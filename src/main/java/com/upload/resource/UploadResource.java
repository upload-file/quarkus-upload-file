package com.upload.resource;

import com.upload.model.UploadFile;
import com.upload.service.FileService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Path("/upload")
@ApplicationScoped
public class UploadResource {

    @Inject
    FileService fileService;

    @POST
    @Path("/csv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadCsv(@MultipartForm FileUploadForm form) {
        try (InputStream inputStream = form.getFileData()) {
            if (inputStream != null) {
                List<UploadFile> files = fileService.processCsvFile(inputStream);
                return Response.ok(files).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("File data is null").build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }


    @POST
    @Path("/excel")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadExcel(@MultipartForm FileUploadForm form) {
        try (InputStream inputStream = form.getFileData()) {
            List<UploadFile> files = fileService.processExcelFile(inputStream);
            return Response.ok(files).build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
