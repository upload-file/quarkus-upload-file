package com.upload.readers;

import com.upload.model.UploadFile;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
@Consumes(MediaType.TEXT_PLAIN)
public class PlainTextMessageBodyReader implements MessageBodyReader<UploadFile> {

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == UploadFile.class && mediaType.toString().startsWith(MediaType.TEXT_PLAIN);
    }

    @Override
    public UploadFile readFrom(Class<UploadFile> type, Type genericType, Annotation[] annotations, MediaType mediaType,
                               MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
            throws IOException, WebApplicationException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(entityStream))) {
            String content = reader.readLine();
            UploadFile uploadFile = new UploadFile();
            uploadFile.setFileName(content);  // Sesuaikan dengan kebutuhan Anda
            return uploadFile;
        } catch (Exception e) {
            throw new WebApplicationException("Error reading text/plain data", e);
        }
    }
}
