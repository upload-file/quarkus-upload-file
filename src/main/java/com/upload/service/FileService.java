package com.upload.service;

import com.upload.model.UploadFile;
import jakarta.enterprise.context.ApplicationScoped;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class FileService {

    public List<UploadFile> processCsvFile(InputStream inputStream) throws IOException {
        List<UploadFile> uploadFiles = new ArrayList<>();
        try (CSVParser parser = CSVFormat.EXCEL.parse(new InputStreamReader(inputStream))) {

            CSVRecord headerRecord = parser.iterator().next();
            int fileNameIndex = -1;
            int fileDataIndex = -1;
            for (int i = 0; i < headerRecord.size(); i++) {
                String columnName = headerRecord.get(i);
                if ("fileName".equalsIgnoreCase(columnName)) {
                    fileNameIndex = i;
                } else if ("fileData".equalsIgnoreCase(columnName)) {
                    fileDataIndex = i;
                }
            }
            if (fileNameIndex == -1 || fileDataIndex == -1) {
                throw new IllegalArgumentException("Column names 'fileName' and 'fileData' not found in CSV header");
            }
            for (CSVRecord record : parser) {
                UploadFile uploadFile = new UploadFile();

                uploadFile.setFileName(record.get(fileNameIndex));
                uploadFile.setFileData(record.get(fileDataIndex));
                uploadFiles.add(uploadFile);
            }
        }
        return uploadFiles;
    }


    public List<UploadFile> processExcelFile(InputStream inputStream) throws IOException {
        List<UploadFile> uploadFiles = new ArrayList<>();
        try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter formatter = new DataFormatter();
            for (Row row : sheet) {
                UploadFile uploadFile = new UploadFile();

                if (row.getPhysicalNumberOfCells() >= 2) {
                    uploadFile.setFileName(formatter.formatCellValue(row.getCell(0)));
                    uploadFile.setFileData(formatter.formatCellValue(row.getCell(1)));
                    uploadFiles.add(uploadFile);
                } else {
                    throw new IOException("Excel file is missing required columns: fileName and fileData");
                }
            }
        }
        return uploadFiles;
    }
}
